package cn.aohan.scheduled.annotation;

import java.lang.annotation.*;

/**
 * @author 傲寒
 * @since 1.0
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Scheduled {

    /**
     * cron
     *
     * @return {@link String}
     */
    String cron() default "";

    /**
     * job数据  必须是spring中的Map《String,Object》类型的bean
     *
     * @return {@link String}
     */
    String jobDataRef() default "";

    /**
     * 耐用性
     *
     * @return boolean
     */
    boolean durability() default false;

    /**
     * 指示计划程序在遇到“恢复”或“故障转移”情况时是否应重新执行作业
     *
     * @return boolean
     */
    boolean recoverable() default false;
}
