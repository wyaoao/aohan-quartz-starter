package cn.aohan.scheduled;


import cn.aohan.scheduled.config.AohanSchedulingConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启用计划
 *
 * @author 傲寒
 * @since  2024/04/16
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({AohanSchedulingConfiguration.class})
public @interface EnableScheduled {



}
